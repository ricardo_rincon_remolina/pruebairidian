import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CardEventComponent } from './card-event/card-event.component';



@NgModule({
  declarations: [
    NavbarComponent,
    CardEventComponent
  ],
  exports:[
    NavbarComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule
  ]
})
export class ComponentsModule { }
