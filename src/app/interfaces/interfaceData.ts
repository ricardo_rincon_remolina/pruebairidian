export interface Events {
    id:      string;
    name:    string;
    markets: Market[];
}

export interface Market {
    id:         string;
    name:       string;
    selections?: Selection[];
}

export interface Selection {
    id:    string;
    name:  string;
    price: number;
}

export interface SelectionModal {
    idEvent:  string;
    idMarket: string;
    id:       string;
    name:     string;
    price:    number;
}