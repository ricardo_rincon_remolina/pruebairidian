import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { ComponentsModule } from '../../components/components.module';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';


import { HttpClientModule } from '@angular/common/http';
import { ServiceEventoService } from './services/service-evento.service';
import { ModalSeleccionesComponent } from './components/modal-selecciones/modal-selecciones.component';

@NgModule({
  declarations: [
    HomePageComponent,
    ModalSeleccionesComponent    
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ComponentsModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    HttpClientModule,
    MatDialogModule   
  ],
  providers:[
    ServiceEventoService
  ]
})
export class HomeModule { }
