import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Events } from '../../../interfaces/interfaceData';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceEventoService {

  private url = environment.api;
  
  constructor( public http: HttpClient) { }

  getAllEvents(): Observable<any>{
    return this.http.get(`${ this.url }`)
    .pipe( 
      map( (dataraw:Events[] )=> {
          console.log("service",dataraw);
          return dataraw;
        })
      );
  }
}
