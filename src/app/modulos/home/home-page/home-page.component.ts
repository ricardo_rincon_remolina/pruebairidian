import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServiceEventoService } from '../services/service-evento.service';
import { Events, Selection, SelectionModal } from '../../../interfaces/interfaceData';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalSeleccionesComponent } from '../components/modal-selecciones/modal-selecciones.component';
import Swal from 'sweetalert2/dist/sweetalert2.js';



@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  eventosData: Events[] = [];
  listObserver$: Array<Subscription> = [];
  selecciones: SelectionModal[] = []

  constructor(private httpService: ServiceEventoService,
              public dialog: MatDialog
              ) { }

    openDialog(idEvento,idmarket,dataSeleccion){

      const validarSeleccion = this.selecciones.find( evento => {
        
        if( evento.idEvent == idEvento && evento.idMarket == idmarket && evento.id == dataSeleccion.id){
          return evento;
        }
      });

      if( validarSeleccion ){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Error: Selección ya existe!'
        });
        return false;
      }else{     
        this.selecciones.push({
          idEvent:  idEvento,
          idMarket: idmarket,
          id:       dataSeleccion.id,
          name:     dataSeleccion.name,
          price:    dataSeleccion.price
        });
        
        const dialogRef = this.dialog.open(ModalSeleccionesComponent, {
          width: '100%',
          data: this.selecciones
        });

        dialogRef.afterClosed()
          .subscribe( result =>{
            if( result ){
              this.selecciones = result;              
            }
        });
      }
    }              
  ngOnInit(): void {
    const eventosObserver$ =this.httpService.getAllEvents()
      .subscribe( (data: Events[]) => {
        this.eventosData = data;        
      });
    this.listObserver$ = [eventosObserver$];          
  }

  desplegarModal(){    
    
    if( this.selecciones.length > 0){
      const dialogRef = this.dialog.open(ModalSeleccionesComponent, {
        width: '100%',
        data: this.selecciones
      });
  
      dialogRef.afterClosed()
        .subscribe( result =>{
          if( result ){
            this.selecciones = result;
          }
      });
    }else{
      Swal.fire({
        icon: 'warning',
        title: 'Oops...',
        text: 'Error: No datos para mostrar'
      });
    }

  }

  ngOnDestroy(): void {    
    this.listObserver$.forEach( u => u.unsubscribe );    
  }
}
