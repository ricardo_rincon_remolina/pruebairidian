import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Events, Selection, SelectionModal } from 'src/app/interfaces/interfaceData';
@Component({
  selector: 'app-modal-selecciones',
  templateUrl: './modal-selecciones.component.html',
  styleUrls: ['./modal-selecciones.component.css']
})
export class ModalSeleccionesComponent implements OnInit {

  constructor(   
       public dialogRef: MatDialogRef<ModalSeleccionesComponent>,    
      @Inject(MAT_DIALOG_DATA) public data: SelectionModal[]) 
      { }

  ngOnInit(): void {
    
  }
  
  onNoClick(index): void {    
    this.data.splice(index,1);    
    this.dialogRef.close(this.data);
  }
}
