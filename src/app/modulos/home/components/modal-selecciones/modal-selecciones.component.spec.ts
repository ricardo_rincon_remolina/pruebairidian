import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSeleccionesComponent } from './modal-selecciones.component';

describe('ModalSeleccionesComponent', () => {
  let component: ModalSeleccionesComponent;
  let fixture: ComponentFixture<ModalSeleccionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalSeleccionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSeleccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
